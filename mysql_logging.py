import argparse
import MySQLdb as mdb


parser = argparse.ArgumentParser(description='Save information in MySQL database.')
parser.add_argument('--temperature', action='store_true', help='Log CPU, Raspi and Room temperature.')
parser.add_argument('--speed',       action='store_true', help='Log upload and download speed.')
args = parser.parse_args()


con = mdb.connect('localhost', 'teuse', 'Mattes12pi', 'logging');
cur = con.cursor()

with con:

    #===========================================================================
    if args.temperature:
        print "Log temperature"
        cur.execute("CREATE TABLE IF NOT EXISTS Temperatures( Id INT PRIMARY KEY AUTO_INCREMENT, \
                     Date DATE, Time TIME, CPU DOUBLE, Raspi DOUBLE, Room DOUBLE, Unit VARCHAR(25) )")

        import lib_system_info as sysInfo
        import lib_temperatures as temperatures
        cpu   = sysInfo.get_temperature()
        temps = temperatures.get_temperatures()
        raspi = temps[1]
        room  = temps[0]

        cur.execute("INSERT INTO Temperatures(Date, Time, CPU, Raspi, Room, Unit)\
                VALUES(CURRENT_DATE, CURRENT_TIME, %.2f, %.2f, %.2f, 'C')" % (cpu,raspi,room) )


    #===========================================================================
    if args.speed:
        print "Log internet speed"
        cur.execute("CREATE TABLE IF NOT EXISTS Internet( Id INT PRIMARY KEY AUTO_INCREMENT, \
                     Date DATE, Time TIME, Download DOUBLE, Upload DOUBLE, Unit VARCHAR(25) )")

        import lib_internet_speed as spLib
        speed = spLib.averageSpeed(1)

        cur.execute("INSERT INTO Internet(Date, Time, Download, Upload, Unit)\
            VALUES(CURRENT_DATE, CURRENT_TIME, %.2f, %.2f, 'Mbit/s')" % (speed[0],speed[1]) )

print "Logging successful..."
