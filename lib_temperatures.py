import re, os, time


# define pathes to 1-wire sensor data
pathes = (
        "/sys/bus/w1/devices/10-000802abf612/w1_slave", # Room
        "/sys/bus/w1/devices/10-000802abfe95/w1_slave"  # GPIO
        )


def get_temperatures():
    # read sensor data
    data = []
    for path in pathes:
        data.append( _read_sensor(path) )
        time.sleep(1)
    return data


# function: read and parse sensor data file
def _read_sensor(path):
   value = 0.0
   try:
      f = open(path, "r")
      line = f.readline()
      if re.match(r"([0-9a-f]{2} ){9}: crc=[0-9a-f]{2} YES", line):
         line = f.readline()
         m = re.match(r"([0-9a-f]{2} ){9}t=([+-]?[0-9]+)", line)
         if m:
            value = float(m.group(2)) / 1000.0
      f.close()
   except (IOError), e:
      print time.strftime("%x %X"), "Error reading", path, ": ", e
   return value

